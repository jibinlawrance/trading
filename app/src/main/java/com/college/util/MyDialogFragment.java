package com.college.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.college.traderapplication.R;

public class MyDialogFragment extends DialogFragment {

    public static MyDialogFragment newInstance() {
        MyDialogFragment frag = new MyDialogFragment();

        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.prompt, null))
                .setTitle("Title");
        // Set ur code

        return builder.create();
    }

}

