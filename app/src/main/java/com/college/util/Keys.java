package com.college.util;



public class Keys {

    public static String ADMIN="http://192.168.0.108/traderapp/admin/";
    public static String USER="http://192.168.0.108/traderapp/user/";
    public static String VENDOR="http://192.168.0.108/traderapp/vendor/";

    public static class URL{
        public static String admin_login=ADMIN+"a_login.php";
        public static String add_vendor=ADMIN+"add_vendor.php";
        public static String all_product=ADMIN+"get_all_product.php";
        public static String all_user=ADMIN+"get_all_user.php";
        public static String all_vendor=ADMIN+"get_all_vendor.php";

        public static String user_login=USER+"u_login.php";
        public static String user_register=USER+"u_register.php";
        public static String get_product_bids=USER+"get_product_bids.php";
        public static String place_bid=USER+"place_bid.php";
        public static String bidder_user_bids=USER+"get_all_bid.php";
        //public static String get_all_product=VENDOR+"get_all_product.php";


        public static String vendor_login=VENDOR+"v_login.php";
        public static String add_product=VENDOR+"add_product.php";
        public static String get_product=VENDOR+"get_all_product.php";
        public static String get_product_bid=VENDOR+"get_product_bid.php";
        public static String accept_bid=VENDOR+"accept_bid.php";
        public static String bidder_details=VENDOR+"get_all_bid.php";



    }
}

