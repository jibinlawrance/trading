package com.college.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMainActivity;
import com.college.pojo.BidsProduct;
import com.college.pojo.Product;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.vendor.BidderDetailsActivity;
import com.college.vendor.SeeProductActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BidsProductAdapter extends RecyclerView.Adapter<BidsProductAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BidsProduct> list;
    private Activity activity;

    public BidsProductAdapter(Context context, ArrayList<BidsProduct> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public BidsProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.bidsproductlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new BidsProductAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final BidsProductAdapter.ViewHolder holder, int position) {
        final BidsProduct product=list.get(position);
        holder.tv_name.setText("Name: "+product.getName());
        holder.tv_amount.setText("Amount: "+ product.getAmount());
        holder.tv_date.setText("Date: "+product.getDate());
//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(context, BidderDetailsActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("b_id",product.getB_id());
//                context.startActivity(intent);
//            }
//        });

        switch (product.getB_status()){
            case "1": holder.button_accept.setText("ACCEPTED"); break;
            case "0": holder.button_accept.setText("ACCEPT");   break;
            case "3": holder.button_accept.setVisibility(View.GONE);    break;
        }


        holder.button_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.accept_bid, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("success").equals("1")){

                                    holder.button_accept.setText("Accepted");


                                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                                Intent intent=new Intent(context, SeeProductActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                context.startActivity(intent);
//                                activity.finish();
                                //((AppCompatActivity)context).finish();


                            }else {
                                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Technical problem arises", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params=new HashMap<>();
                        params.put("b_id",product.getB_id());
                        params.put("b_status",product.getB_status());
                        return params;
                    }
                };

                AppController.getInstance().add(request);
            }
        });
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_amount,tv_date;
        Button button_accept;
        CardView cardView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_user_name);
            this.tv_amount=itemView.findViewById(R.id.txt_amount);
            this.tv_date=itemView.findViewById(R.id.txt_date);
            this.button_accept=itemView.findViewById(R.id.btn_accept);
            this.cardView=itemView.findViewById(R.id.bids_card_view);


        }
    }
}
