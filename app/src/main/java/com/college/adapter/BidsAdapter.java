package com.college.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.pojo.Bids;
import com.college.pojo.Product;
import com.college.traderapplication.LoginActivity;
import com.college.traderapplication.PlaceorderActivity;
import com.college.traderapplication.R;
import com.college.traderapplication.UserMenuActivity;
import com.college.traderapplication.UsersActivity;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.MyDialogFragment;
import com.college.util.SharedPreference;
import com.college.vendor.BidsProductActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class BidsAdapter extends RecyclerView.Adapter<BidsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Bids> list;
    private Activity activity;

    public BidsAdapter(Context context, ArrayList<Bids> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public BidsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.bidslist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new BidsAdapter.ViewHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull BidsAdapter.ViewHolder holder, int position) {
        final Bids bids =list.get(position);
        holder.tv_title.setText(bids.getName());
        holder.tv_desc.setText(bids.getDescription());
        holder.tv_prize.setText(bids.getPrize());
        Picasso.with(context).load( Keys.VENDOR+ bids.getPath()).into(holder.imageView);


        holder.button_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PlaceorderActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("p_id",bids.getP_id());
                context.startActivity(intent);
            }
        });

    }


//        holder.cd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(context, UsersActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);
//            }
//        });
//




    @Override
    public int getItemCount() {

        return list.size();
    }
    @Override
    public long getItemId(int position) {

        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title,tv_prize,tv_desc;
        ImageView imageView;
        CardView cd;
        Button button_place;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_title=itemView.findViewById(R.id.txt_p_name);
            this.tv_prize=itemView.findViewById(R.id.txt_p_price);
            this.tv_desc=itemView.findViewById(R.id.txt_p_desc);
            this.imageView=itemView.findViewById(R.id.p_image);
            this.cd=itemView.findViewById(R.id.card_view);
            this.button_place=itemView.findViewById(R.id.btn_place);

            this.button_place.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}
