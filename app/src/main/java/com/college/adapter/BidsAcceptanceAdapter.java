package com.college.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.BidsAccept;
import com.college.pojo.Product;
import com.college.traderapplication.R;
import com.college.vendor.BidsProductActivity;

import java.util.ArrayList;

public class BidsAcceptanceAdapter extends RecyclerView.Adapter<BidsAcceptanceAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BidsAccept> list;

    public BidsAcceptanceAdapter(Context context, ArrayList<BidsAccept> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public BidsAcceptanceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.bidsacceptlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new BidsAcceptanceAdapter.ViewHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull BidsAcceptanceAdapter.ViewHolder holder, int position) {
        BidsAccept accept=list.get(position);
        holder.tv_name.setText("Name: "+accept.getName());
        holder.tv_p_name.setText("Product Name: "+accept.getProduct_name());
        holder.tv_prize.setText("Amount: "+accept.getPrize());
        holder.tv_status.setText(accept.getStatus());
        switch (accept.getStatus()){
            case "1": holder.tv_status.setText("Accepted"); break;
            case "0": holder.tv_status.setText("Not Accepted");
                       holder.tv_status.setTextColor(Color.RED);break;

        }
























    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public long getItemId(int position) {

        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_p_name,tv_prize,tv_status;
        CardView cd;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_name);
            this.tv_p_name=itemView.findViewById(R.id.txt_p_name);
            this.tv_prize=itemView.findViewById(R.id.txt_prize);
            this.tv_status=itemView.findViewById(R.id.txt_status);
            this.cd=itemView.findViewById(R.id.card_view);


        }
    }
}
