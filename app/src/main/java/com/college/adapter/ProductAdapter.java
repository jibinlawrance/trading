package com.college.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.Product;
import com.college.traderapplication.R;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.college.vendor.BidsProductActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class
ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Product> list;

    public ProductAdapter(Context context, ArrayList<Product> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.productlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new ProductAdapter.ViewHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.ViewHolder holder, int position) {
        Product product=list.get(position);
        holder.tv_title.setText(product.getP_name());
        holder.tv_desc.setText(product.getP_desc());
        holder.tv_prize.setText(product.getP_prize());
        Picasso.with(context).load( Keys.VENDOR+ product.getGetPath()).into(holder.imageView);


       // holder.imageView.setImageResource(product.getImage());
//        holder.cd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(context, BidsProductActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);
//
//            }
//        });


    }

    @Override
    public int getItemCount() {

        return list.size();
    }
    @Override
    public long getItemId(int position) {

        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title,tv_desc,tv_prize;
        ImageView imageView;
        CardView cd;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_title=itemView.findViewById(R.id.txt_p_name);
            this.tv_desc=itemView.findViewById(R.id.txt_p_desc);
            this.tv_prize=itemView.findViewById(R.id.txt_p_price);
            this.imageView=itemView.findViewById(R.id.p_image);
            this.cd=itemView.findViewById(R.id.card_view);


        }
    }
}
