package com.college.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.BidderDetails;
import com.college.traderapplication.R;

import java.util.ArrayList;

public class BidderdetailsAdapter extends RecyclerView.Adapter<BidderdetailsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BidderDetails> list;


    public BidderdetailsAdapter(Context context, ArrayList<BidderDetails> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public BidderdetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.bidderdetailslist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new BidderdetailsAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull BidderdetailsAdapter.ViewHolder holder, int position) {
        BidderDetails details=list.get(position);
        holder.tv_name.setText("Name: "+details.getName());
        holder.tv_p_name.setText("Product Name: "+details.getProductName());
        holder.tv_prize.setText("Product Price: "+details.getPrize());
        holder.tv_date.setText("Date: "+details.getDate());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_p_name,tv_prize,tv_date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_bidder_name);
            this.tv_p_name=itemView.findViewById(R.id.txt_bidder_product_name);
            this.tv_prize=itemView.findViewById(R.id.txt_bidder_product_prize);
            this.tv_date=itemView.findViewById(R.id.txt_bidder_date);
        }
    }
}
