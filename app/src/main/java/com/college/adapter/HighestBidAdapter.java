package com.college.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.HighestBid;
import com.college.traderapplication.R;

import java.util.ArrayList;

public class HighestBidAdapter extends RecyclerView.Adapter<HighestBidAdapter.ViewHolder> {

    private Context context;
    private ArrayList<HighestBid> list;

    public HighestBidAdapter(Context context, ArrayList<HighestBid> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HighestBidAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.highestbidlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new HighestBidAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull HighestBidAdapter.ViewHolder holder, int position) {
        HighestBid bid=list.get(position);
        holder.tv_name.setText("Name: "+bid.getName());
        holder.tv_amount.setText("Amount: "+bid.getAmount());
        holder.tv_time.setText("Time: "+bid.getTime());
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_amount,tv_time;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_u_name);
            this.tv_amount=itemView.findViewById(R.id.txt_bids_amount);
            this.tv_time=itemView.findViewById(R.id.txt_bids_time);

        }
    }
}
