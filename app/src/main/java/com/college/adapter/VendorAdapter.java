package com.college.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.Vendor;
import com.college.traderapplication.R;

import java.util.ArrayList;

public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.ViewHolder> {


    private Context context;
    private ArrayList<Vendor> list;


    public VendorAdapter(Context context, ArrayList<Vendor> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public VendorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.vendorlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new VendorAdapter.ViewHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull VendorAdapter.ViewHolder holder, int position) {
        Vendor vendor=list.get(position);
        holder.tv_name.setText("Name: "+vendor.getName());
        holder.tv_email.setText("Email: "+vendor.getEmail());
        holder.tv_phone.setText("Phone: "+vendor.getPhone());



    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    @Override
    public long getItemId(int position) {
        return position;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_email,tv_phone,tv_company;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_vendor_name);
            this.tv_email=itemView.findViewById(R.id.txt_vendor_email);
            this.tv_phone=itemView.findViewById(R.id.txt_vendor_phone);

        }
    }
}
