package com.college.vendor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.college.admin.AdminMainActivity;
import com.college.traderapplication.MainMenuActivity;
import com.college.traderapplication.R;
import com.college.util.SharedPreference;

public class MenuActivity extends AppCompatActivity {

    Button button_see_product,button_add_product,button_logout;
    String value="2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().setTitle("Menu");
        Log.i("NIK", "v_id => " + SharedPreference.get("v_id"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        button_see_product=findViewById(R.id.btn_see_product);
        button_add_product=findViewById(R.id.btn_add_product);
        button_logout=findViewById(R.id.btn_logout);
        if (SharedPreference.contains("value")){
            value=SharedPreference.get("value");
        }
        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("v_email") && SharedPreference.contains("v_password")){
                    Intent intent=new Intent(MenuActivity.this,VendorLoginActivity.class);
                    SharedPreference.removeKey("v_email");
                    SharedPreference.removeKey("v_password");
                    SharedPreference.save("value","0");
                    startActivity(intent);
                    finish();
                }
            }
        });

        button_see_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this,SeeProductActivity.class);
                startActivity(intent);
            }
        });
        button_add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuActivity.this,AddProductActivity.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog=new AlertDialog.Builder(MenuActivity.this);
        dialog.setMessage("Do you want to close this app");
        dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}

