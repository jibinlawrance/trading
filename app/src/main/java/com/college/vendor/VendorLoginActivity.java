package com.college.vendor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMainActivity;
import com.college.traderapplication.MainMenuActivity;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VendorLoginActivity extends AppCompatActivity {

    Button button_login;
    EditText editText_email,editText_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_login);
        editText_email=findViewById(R.id.edt_v_email);
        editText_pass=findViewById(R.id.edt_v_pass);
        button_login=findViewById(R.id.btn_v_login);
        getSupportActionBar().setTitle("Vendor Login");
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String v_email=editText_email.getText().toString().trim();
               String v_password=editText_pass.getText().toString().trim();
               if (v_email.equals("") || v_password.equals("")){
                   Toast.makeText(VendorLoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
               }else if (!Patterns.EMAIL_ADDRESS.matcher(v_email).matches()){
                   editText_email.setError("Invalid Email ID");
               }else {
                   login(v_email,v_password);
               }
            }
        });


    }

    private void login(final String v_email, final String v_password) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.vendor_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONObject data = jsonObject.getJSONObject("data");
                        Toast.makeText(VendorLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(VendorLoginActivity.this, MenuActivity.class);
                        SharedPreference.save("v_id", data.getString("v_id"));
                        SharedPreference.save("v_email",v_email);
                        SharedPreference.save("v_password",v_password);
                        SharedPreference.save("value","2");
                        clear();
                        startActivity(intent);
                        finish();

                    }else {
                        Toast.makeText(VendorLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(VendorLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("v_email",v_email);
                params.put("v_password",v_password);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_email.setText("");
        editText_pass.setText("");
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(VendorLoginActivity.this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
