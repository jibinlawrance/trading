package com.college.vendor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.BidsProductAdapter;
import com.college.adapter.VendorProductAdapter;
import com.college.pojo.BidsProduct;;
import com.college.pojo.VendorProduct;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BidsProductActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    BidsProductAdapter adapter;
    ArrayList<BidsProduct> list;
    LinearLayoutManager layoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bids_product);
        recyclerView=findViewById(R.id.recycler_bids_list);
        getSupportActionBar().setTitle("Bid");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
        Intent i=getIntent();
        String p_id=i.getStringExtra("p_id");
        bidProduct(p_id);
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        list.add(new BidsProduct("John Mathew","25,000Rs","02-02-2020"));
//        adapter=new BidsProductAdapter(getApplicationContext(),list);
//        recyclerView.setAdapter(adapter);


    }

    private void bidProduct(final String p_id) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.get_product_bid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        int status = 0;     //0->NOt accepted, 1->Accepted
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new BidsProduct(jsonObject1.getString("u_name"),
                                    jsonObject1.getString("b_amount"),
                                    jsonObject1.getString("b_time"),
                                    jsonObject1.getString("b_id"),
                                    jsonObject1.getString("b_status")));
                            if(jsonObject1.getString("b_status").equals("1"))
                                status = 1;
                        }
                        if(status == 1){
                            for(int i=0; i<list.size(); i++){
                                String b_status = list.get(i).getB_status();
                                if(b_status.equals("0"))
                                    list.get(i).setB_status("3");
                            }
                        }

                        BidsProductAdapter adapter=new BidsProductAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("p_id",p_id);
                return params;
            }
        };

        AppController.getInstance().add(request);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }



}
