package com.college.vendor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.BidderdetailsAdapter;
import com.college.adapter.VendorProductAdapter;
import com.college.pojo.BidderDetails;
import com.college.pojo.VendorProduct;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BidderDetailsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    BidderdetailsAdapter adapter;
    ArrayList<BidderDetails> list;
    LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bidder_details);
        recyclerView=findViewById(R.id.recycler_bidder_list);
        getSupportActionBar().setTitle("Bidder Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
        Intent i=getIntent();
        String b_id=i.getStringExtra("b_id");
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        list.add(new BidderDetails("Nikita","HP Laptop","25,000 Rs","03-02-2020"));
//        adapter=new BidderdetailsAdapter(getApplicationContext(),list);
//        recyclerView.setAdapter(adapter);

        bidderDetails(b_id);


    }

    private void bidderDetails(String b_id) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.bidder_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);


                        }



                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
