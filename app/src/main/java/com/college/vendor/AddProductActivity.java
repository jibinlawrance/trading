package com.college.vendor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.college.traderapplication.R;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import net.gotev.uploadservice.BuildConfig;
import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class AddProductActivity extends AppCompatActivity {

    EditText edt_p_name,edt_p_desc,edt_p_prize;
    ImageView imageview;
    Button button_add;
    private static final int MY_PERMISSION_REQUEST_CODE=1001;
    private String selectedImagePath="";
    private static final String IMAGE_DIRECTORY="/trader app";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        getSupportActionBar().setTitle("Add Product");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        UploadService.NAMESPACE = "com.college.traderapplication";
        edt_p_name=findViewById(R.id.edt_product_name);
        edt_p_desc=findViewById(R.id.edt_product_desc);
        edt_p_prize=findViewById(R.id.edt_product_prize);
        button_add=findViewById(R.id.btn_add);
        imageview=findViewById(R.id.p_img);
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String p_name=edt_p_name.getText().toString().trim();
               String p_description=edt_p_desc.getText().toString().trim();
               String p_price=edt_p_prize.getText().toString().trim();
               if (p_name.equals("") || p_description.equals("") || p_price.equals("")){
                   Toast.makeText(AddProductActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
               }else{
                   add(p_name,p_description,p_price,selectedImagePath);
               }


            }
        });


        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)  + ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_EXTERNAL_STORAGE) + ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        String[] permissions = {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions(permissions,MY_PERMISSION_REQUEST_CODE);
                    }else {
                        takePhotoFromCamera();
                    }
                }else {
                    takePhotoFromCamera();
                }
            }

        });


    }

    private void add(String p_name, String p_description, String p_price, String selectedImagePath) {
        try {
            Log.i("nik",selectedImagePath);
            String image= UUID.randomUUID().toString();
            UploadNotificationConfig config = new UploadNotificationConfig();
            config.getCompleted().autoClear = true;
            config.setTitleForAllStatuses("photo");
            config.setIconForAllStatuses(R.mipmap.ic_launcher);
            new MultipartUploadRequest(getApplicationContext(),image, Keys.URL.add_product)
                    .addFileToUpload(selectedImagePath,"p_photo")
                    .addParameter("p_name",p_name)
                    .addParameter("p_description",p_description)
                    .addParameter("p_price",p_price)
                    .addParameter("v_id", SharedPreference.get("v_id"))
                    .setMaxRetries(5)
                    .setNotificationConfig(config)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {
                            Log.i("nik","on progress upload");
                        }

                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {

                            Log.i("nik","on error upload");
                        }

                        @Override
                        public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {

                            Log.i("nik","on completed upload");
                            Log.i("nik",serverResponse.getBodyAsString());
                            try {
                                JSONObject json=new JSONObject(serverResponse.getBodyAsString());
                                if (json.getString("success").equals("1")){
                                    Toast.makeText(context, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                                    clear();

                                }else {
                                    Toast.makeText(context, "Unsuccessfully uploaded", Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            Log.i("NIK", "onCancelled");
                        }
                    })
                    .startUpload();
            // Toast.makeText(getActivity(), "Donate Medicine Successfully", Toast.LENGTH_SHORT).show();

        }catch (Exception e){
            // Toast.makeText(getActivity(), "Donate Medicine Failed", Toast.LENGTH_SHORT).show();
        }




    }

    private void clear() {
        edt_p_name.setText("");
        edt_p_desc.setText("");
        edt_p_prize.setText("");
        imageview.setImageResource(0);
        imageview.setImageResource(R.drawable.ic_photo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);



    }





    private String saveToGallery(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File photo = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        if (!photo.exists()) {
            photo.mkdirs();
        }

        try {
            File f = new File(photo, Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            selectedImagePath = f.getAbsolutePath();
            Log.d("TAG", "File Saved::----&gt" + f.getAbsolutePath());
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {


            case CAMERA:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    saveToGallery(photo);
                    imageview.setImageBitmap(photo);

                }
                break;
        }

    }

}
