package com.college.vendor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.ProductAdapter;
import com.college.adapter.VendorAdapter;
import com.college.adapter.VendorProductAdapter;
import com.college.pojo.Product;
import com.college.pojo.VendorProduct;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SeeProductActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    VendorProductAdapter adapter;
    ArrayList<VendorProduct> list;
    LinearLayoutManager layoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_product);
        recyclerView=findViewById(R.id.recycler_vendor_product_list);
        getSupportActionBar().setTitle("Products");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
        productlist();

    }

    private void productlist() {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.get_product, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new VendorProduct(
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_photo"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_price")));


                        }
                        VendorProductAdapter adapter=new VendorProductAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("v_id", SharedPreference.get("v_id"));
                return params;
            }
        };

        AppController.getInstance().add(request);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }



}
