package com.college.adminfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.UserAdapter;
import com.college.adapter.VendorAdapter;
import com.college.pojo.User;
import com.college.pojo.Vendor;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SeeVendorFragment extends Fragment {

    ArrayList<Vendor> list;
    VendorAdapter adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_see_vendor,container,false);
        recyclerView=view.findViewById(R.id.recycler_vendor_list);
        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
        vendorList();
        return view;

    }

    private void vendorList() {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.all_vendor, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Vendor(jsonObject1.getString("v_name"),
                                    jsonObject1.getString("v_email"),
                                    jsonObject1.getString("v_phone")));


                        }
                        VendorAdapter adapter=new VendorAdapter(getActivity(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);

    }
}
