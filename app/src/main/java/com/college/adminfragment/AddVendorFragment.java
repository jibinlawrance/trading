package com.college.adminfragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMainActivity;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddVendorFragment extends Fragment {


    EditText editText_name,editText_email,editText_pass,editText_conf_pass,editText_phone;
    Button button_add;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_add_vendor,container,false);
        editText_name=view.findViewById(R.id.edt_v_name);
        editText_email=view.findViewById(R.id.edt_v_email);
        editText_pass=view.findViewById(R.id.edt_v_password);
        editText_conf_pass=view.findViewById(R.id.edt_v_confirm_password);
        editText_phone=view.findViewById(R.id.edt_v_phone);
        button_add=view.findViewById(R.id.btn_add_vendor);
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String v_name=editText_name.getText().toString().trim();
                String v_email=editText_email.getText().toString().trim();
                String v_password=editText_pass.getText().toString().trim();
                String v_confirm_pass=editText_conf_pass.getText().toString().trim();
                String v_phone=editText_phone.getText().toString().trim();
                if (v_name.equals("") || v_email.equals("") || v_password.equals("") || v_confirm_pass.equals("") || v_phone.equals("")){
                    Toast.makeText(getContext(), "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(v_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else if (!v_confirm_pass.equals(v_password)){
                    editText_conf_pass.setError("please confirm the password");
                }else if (v_phone.length()!=10){
                    editText_phone.setError("phone number must be 10 digit");
                }else {
                    addVendor(v_name,v_email,v_password,v_phone);
                }



            }
        });




        return view;
    }

    private void addVendor(final String v_name, final String v_email, final String v_password, final String v_phone) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.add_vendor, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        clear();


                    }else {
                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("v_name",v_name);
                params.put("v_email",v_email);
                params.put("v_password",v_password);
                params.put("v_phone",v_phone);
                return params;
            }
        };
        AppController.getInstance().add(request);


    }

    private void clear() {
        editText_name.setText("");
        editText_email.setText("");
        editText_pass.setText("");
        editText_conf_pass.setText("");
        editText_phone.setText("");
    }
}
