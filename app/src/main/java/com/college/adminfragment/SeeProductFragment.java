package com.college.adminfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.ProductAdapter;
import com.college.adapter.UserAdapter;
import com.college.pojo.Product;
import com.college.pojo.User;
import com.college.traderapplication.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SeeProductFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Product> list;
    ProductAdapter adapter;
    LinearLayoutManager layoutManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_product,container,false);
        recyclerView=view.findViewById(R.id.recycler_product_list);
        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        list.add(new Product("Hp Laptop","4GB RAM 1TB Hard Disk","30,000Rs",R.drawable.traderlogo));
//        adapter=new ProductAdapter(getContext(),list);
//        recyclerView.setAdapter(adapter);

        showProduct();

        return view;
    }

    private void showProduct() {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.get_product, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Product(jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_photo"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_price")));


                        }
                        ProductAdapter adapter=new ProductAdapter(getActivity(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);
    }
}
