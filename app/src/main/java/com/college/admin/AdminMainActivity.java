package com.college.admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.college.adminfragment.AddVendorFragment;
import com.college.adminfragment.SeeProductFragment;
import com.college.adminfragment.SeeUsersFragment;
import com.college.adminfragment.SeeVendorFragment;
import com.college.traderapplication.MainMenuActivity;
import com.college.traderapplication.R;
import com.college.util.SharedPreference;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.TextView;

public class AdminMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener {

    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawer;
    TextView tv_login;
    String value="1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        getSupportActionBar().setTitle("Add Vendor");
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        tv_login=headerView.findViewById(R.id.txt_a_email);

        if (SharedPreference.contains("value")){
            value=SharedPreference.get("value");
        }
        Intent i=getIntent();
        String email=i.getStringExtra("a_email");
        tv_login.setText(email);



        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawer,toolbar,
                R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new AddVendorFragment()).addToBackStack(getString(R.string.menu_add_vendor)).commit();
            navigationView.setCheckedItem(R.id.nav_add_vendor);

        }
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_logout:
                if (SharedPreference.contains("a_email") && SharedPreference.contains("a_password")){
                    Intent intent=new Intent(AdminMainActivity.this,AdminLoginActivity.class);
                    SharedPreference.removeKey("a_email");
                    SharedPreference.removeKey("a_password");
                    SharedPreference.save("value","0");
                    startActivity(intent);
                    finish();

                }
                break;
            case android.R.id.home:
                Intent intent=new Intent(AdminMainActivity.this,MainMenuActivity.class);
                startActivity(intent);



        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_add_vendor:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new AddVendorFragment()).commit();
                getSupportActionBar().setTitle(R.string.menu_add_vendor);
                break;
            case R.id.nav_see_vendor:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new SeeVendorFragment()).commit();
                getSupportActionBar().setTitle(R.string.menu_see_vendor);
                break;
            case R.id.nav_see_users:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new SeeUsersFragment()).commit();
                getSupportActionBar().setTitle(R.string.menu_users);
                break;
            case R.id.nav_products:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new SeeProductFragment()).commit();
                getSupportActionBar().setTitle(R.string.menu_product);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog=new AlertDialog.Builder(AdminMainActivity.this);
        dialog.setMessage("Do you want to close this app");
        dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    //    @Override
//    public void onBackPressed() {
//
//        Intent intent = new Intent(AdminMainActivity.this, MainMenuActivity.class);
//        startActivity(intent);
//        finish();
//
//    }

//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        }
//        else {
//            int fragments = getSupportFragmentManager().getBackStackEntryCount();
//            if (fragments == 1) {
//                finish();
//            } else if (getFragmentManager().getBackStackEntryCount() > 1) {
//                getFragmentManager().popBackStack();
//            } else {
//
//                super.onBackPressed();
//            }
//        }


    @Override
    public void onBackStackChanged() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment =
                fragmentManager.findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof AddVendorFragment) {
            setTitle(R.string.menu_add_vendor);

        } else if(currentFragment instanceof SeeVendorFragment) {
            setTitle(R.string.menu_see_vendor);

        } else if(currentFragment instanceof SeeUsersFragment) {
            setTitle(R.string.menu_users);
        } else if(currentFragment instanceof SeeProductFragment) {
            setTitle(R.string.menu_product);
        }


    }
}
