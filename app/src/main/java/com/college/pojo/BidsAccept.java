package com.college.pojo;

public class BidsAccept {
    private String name;
    private String product_name;
    private String prize;
    private String status;

    public BidsAccept(String name, String product_name, String prize, String status) {
        this.name = name;
        this.product_name = product_name;
        this.prize = prize;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
