package com.college.pojo;

public class Bids {
    private String p_id;
    private String name;
    private String path;
    private String description;
    private String prize;


    public Bids(String p_id, String name, String path, String description, String prize) {
        this.p_id = p_id;
        this.name = name;
        this.path = path;
        this.description = description;
        this.prize = prize;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }
}
