package com.college.pojo;

public class VendorProduct {

    private String p_id;
    private String p_name;
    private String image;
    private String p_desc;
    private String p_prize;

    public VendorProduct(String p_id,String p_name, String image, String p_desc, String p_prize) {
        this.p_id=p_id;
        this.p_name = p_name;
        this.image = image;
        this.p_desc = p_desc;
        this.p_prize = p_prize;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getP_desc() {
        return p_desc;
    }

    public void setP_desc(String p_desc) {
        this.p_desc = p_desc;
    }

    public String getP_prize() {
        return p_prize;
    }

    public void setP_prize(String p_prize) {
        this.p_prize = p_prize;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }
}
