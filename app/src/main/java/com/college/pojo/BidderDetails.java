package com.college.pojo;

public class BidderDetails {
    private String name;
    private String productName;
    private String prize;
    private String date;


    public BidderDetails(String name, String productName, String prize, String date) {
        this.name = name;
        this.productName = productName;
        this.prize = prize;
        this.date = date;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
