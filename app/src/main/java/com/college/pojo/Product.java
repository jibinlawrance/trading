package com.college.pojo;

public class Product {

    private String p_name;
    private String getPath;
    private String p_desc;
    private String p_prize;

    public Product(String p_name, String getPath, String p_desc, String p_prize) {
        this.p_name = p_name;
        this.getPath = getPath;
        this.p_desc = p_desc;
        this.p_prize = p_prize;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getGetPath() {
        return getPath;
    }

    public void setGetPath(String getPath) {
        this.getPath = getPath;
    }

    public String getP_desc() {
        return p_desc;
    }

    public void setP_desc(String p_desc) {
        this.p_desc = p_desc;
    }

    public String getP_prize() {
        return p_prize;
    }

    public void setP_prize(String p_prize) {
        this.p_prize = p_prize;
    }
}
