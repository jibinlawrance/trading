package com.college.pojo;

public class BidsProduct {
    private String name;
    private String amount;
    private String date;
    private String b_id;
    private String b_status;

    public BidsProduct(String name, String amount, String date, String b_id, String b_status) {
        this.name = name;
        this.amount = amount;
        this.date = date;
        this.b_id = b_id;
        this.b_status = b_status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getB_id() {
        return b_id;
    }

    public void setB_id(String b_id) {
        this.b_id = b_id;
    }

    public String getB_status() {
        return b_status;
    }

    public void setB_status(String b_status) {
        this.b_status = b_status;
    }
}
