package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMainActivity;
import com.college.util.SharedPreference;
import com.college.vendor.MenuActivity;
import com.college.vendor.VendorLoginActivity;

public class MainMenuActivity extends AppCompatActivity {

    Button button_admin,button_vendor,button_user;
    String value="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        getSupportActionBar().setTitle("Menu");
        button_admin=findViewById(R.id.btn_admin);
        button_vendor=findViewById(R.id.btn_vendor);
        button_user=findViewById(R.id.btn_user);
        if (SharedPreference.contains("value"))
            value=SharedPreference.get("value");



        if(value.equals("1")){
            Intent intent=new Intent(MainMenuActivity.this,AdminMainActivity.class);
            startActivity(intent);
            finish();
        }else if(value.equals("2")){
            Intent intent=new Intent(MainMenuActivity.this,MenuActivity.class);
            startActivity(intent);
            finish();
        }else if(value.equals("3")){
            Intent intent=new Intent(MainMenuActivity.this,UserMenuActivity.class);
            startActivity(intent);
            finish();
        }
        button_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("a_email") && SharedPreference.contains("a_password")){
                    Intent intent=new Intent(MainMenuActivity.this, AdminMainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(MainMenuActivity.this, AdminLoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        button_vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("v_email") && SharedPreference.contains("v_password")){
                    Intent intent=new Intent(MainMenuActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(MainMenuActivity.this, VendorLoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        button_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("u_email") && SharedPreference.contains("u_password")){
                    Intent intent=new Intent(MainMenuActivity.this, UserMenuActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent=new Intent(MainMenuActivity.this,LoginActivity.class);
                    startActivity(intent);
                }


            }
        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
