package com.college.traderapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.college.admin.AdminMainActivity;
import com.college.util.SharedPreference;

public class UserMenuActivity extends AppCompatActivity {

    Button button_product,button_bid,button_logout;
    String value="3";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_menu);
        getSupportActionBar().setTitle("Menu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        button_product=findViewById(R.id.btn_product);
        button_bid=findViewById(R.id.btn_bids);
        button_logout=findViewById(R.id.btn_logout);
        if (SharedPreference.contains("value")){
            value=SharedPreference.get("value");


        }



        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("u_email") && SharedPreference.contains("u_password")){
                    Intent intent=new Intent(UserMenuActivity.this,LoginActivity.class);
                    SharedPreference.removeKey("u_email");
                    SharedPreference.removeKey("u_password");
                    SharedPreference.save("value","0");
                    startActivity(intent);
                    finish();
                }
            }
        });
        button_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(UserMenuActivity.this,ProductBidActivity.class);
                //intent.putExtra("u_id",u_id);
                startActivity(intent);
            }
        });
        button_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(UserMenuActivity.this,PreviousbidActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog=new AlertDialog.Builder(UserMenuActivity.this);
        dialog.setMessage("Do you want to close this app");
        dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
