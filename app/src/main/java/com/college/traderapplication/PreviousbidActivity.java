package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.BidsAcceptanceAdapter;
import com.college.adapter.BidsAdapter;
import com.college.adapter.HighestBidAdapter;
import com.college.pojo.Bids;
import com.college.pojo.BidsAccept;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PreviousbidActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    BidsAcceptanceAdapter adapter;
    ArrayList<BidsAccept> list;
    LinearLayoutManager layoutManager;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previousbid);
        recyclerView=findViewById(R.id.recycler_previous_list);
        getSupportActionBar().setTitle("Previous Bids");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
        final String u_id= SharedPreference.get("u_id");
        previousBids(u_id);


    }

    private void previousBids(final String u_id) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.bidder_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new BidsAccept(jsonObject1.getString("u_name"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("b_amount"),
                                    jsonObject1.getString("b_status")));


                        }
                        BidsAcceptanceAdapter adapter=new BidsAcceptanceAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_id",u_id);
                return params;
            }
        };

        AppController.getInstance().add(request);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
