package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMainActivity;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText editText_name,editText_email,editText_pass,editText_conf_pass,editText_phone;
    Button button_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editText_name=findViewById(R.id.edt_u_name);
        editText_email=findViewById(R.id.edt_u_email);
        editText_pass=findViewById(R.id.edt_u_pass);
        editText_conf_pass=findViewById(R.id.edt_conf_pass);
        editText_phone=findViewById(R.id.edt_u_phone);
        button_register=findViewById(R.id.btn_u_register);
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u_name=editText_name.getText().toString().trim();
                String u_email=editText_email.getText().toString().trim();
                String u_password=editText_pass.getText().toString().trim();
                String u_conf_pass=editText_conf_pass.getText().toString().trim();
                String u_phone=editText_phone.getText().toString().trim();

                if (u_name.equals("") || u_email.equals("") || u_password.equals("") || u_conf_pass.equals("") || u_phone.equals("")){
                    Toast.makeText(RegisterActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(u_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else if (!u_conf_pass.equals(u_password)){
                    editText_conf_pass.setError("please confirm the password");
                }else if (u_phone.length()!=10){
                    editText_phone.setError("phone must be 10 digits");
                }else {
                    login(u_name,u_email,u_password,u_phone);
                }
            }
        });



    }

    private void login(final String u_name, final String u_email, final String u_password, final String u_phone) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.user_register, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                        clear();



                    }else {
                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(RegisterActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_name",u_name);
                params.put("u_email",u_email);
                params.put("u_password",u_password);
                params.put("u_phone",u_phone);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_name.setText("");
        editText_email.setText("");
        editText_pass.setText("");
        editText_conf_pass.setText("");
        editText_phone.setText("");
    }
}
