package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.HighestBidAdapter;
import com.college.pojo.HighestBid;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlaceorderActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    Button button_place_order;
    ArrayList<HighestBid> list;
    String p_id;
    LinearLayoutManager layoutManager;
    CardView cd;
    EditText editText_amount;
    String value="1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placeorder);
        recyclerView=findViewById(R.id.recycler_list);
        button_place_order=findViewById(R.id.btn_submit);
        editText_amount=findViewById(R.id.edt_amount);
        cd=findViewById(R.id.card_submit);
        getSupportActionBar().setTitle("Place Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        Intent i=getIntent();
        p_id=i.getStringExtra("p_id");
        final String u_id= SharedPreference.get("u_id");
        Log.i("nik","p_id"+p_id);
        Log.i("nik","u_id"+u_id);

        list=new ArrayList<>();
        //userList(p_id);

        button_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String b_amount=editText_amount.getText().toString().trim();

                if (b_amount.equals("")){
                    Toast.makeText(PlaceorderActivity.this, "please enter amount", Toast.LENGTH_SHORT).show();
                }else{
                    placeOrder(u_id,p_id,b_amount);

                }

            }
        });





    }

    @Override
    protected void onStart() {
        userList(p_id);
        super.onStart();

    }

    private void placeOrder(final String u_id, final String p_id, final String b_amount) {
        StringRequest request=new StringRequest(Request.Method.POST,Keys.URL.place_bid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        cd.setVisibility(View.GONE);
                        clear();
                        Toast.makeText(PlaceorderActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


                    }else {
                        Toast.makeText(PlaceorderActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(PlaceorderActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_id",u_id);
                params.put("p_id",p_id);
                params.put("b_amount",b_amount);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_amount.setText("");
    }

    private void userList(final String p_id) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.get_product_bids, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    Log.i("list",response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            if(jsonObject1.getString("u_id").equals(SharedPreference.get("u_id")))
                                cd.setVisibility(View.GONE);
                            list.add(new HighestBid(
                                    jsonObject1.getString("u_name"),
                                    jsonObject1.getString("b_amount"),
                                    jsonObject1.getString("b_time")));

                        }
                        HighestBidAdapter adapter=new HighestBidAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);

                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("p_id",p_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
