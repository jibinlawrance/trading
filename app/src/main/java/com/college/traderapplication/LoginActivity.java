package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMainActivity;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {


    Button button_login,button_register;
    EditText editText_email,editText_pass;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editText_email=findViewById(R.id.edt_email);
        editText_pass=findViewById(R.id.edt_pass);
        button_login=findViewById(R.id.btn_login);
        button_register=findViewById(R.id.btn_register);
        getSupportActionBar().setTitle("User Login");
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String u_email=editText_email.getText().toString().trim();
               String u_password=editText_pass.getText().toString().trim();
               if (u_email.equals("") || u_password.equals("")){
                   Toast.makeText(LoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
               }else if (!Patterns.EMAIL_ADDRESS.matcher(u_email).matches()){
                   editText_email.setError("Invalid Email ID");
               }else {
                   login(u_email,u_password);
               }
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void login(final String u_email, final String u_password) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.user_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String u_id=jsonObject1.getString("u_id");

                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(LoginActivity.this, UserMenuActivity.class);
                        //intent.putExtra("u_id",u_id);
                        SharedPreference.save("u_id",u_id);
                        SharedPreference.save("u_email",u_email);
                        SharedPreference.save("u_password",u_password);
                        SharedPreference.save("value","3");
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_email",u_email);
                params.put("u_password",u_password);
                return params;
            }
        };
        AppController.getInstance().add(request);



    }

    private void clear() {
        editText_email.setText("");
        editText_pass.setText("");
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(LoginActivity.this,MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
