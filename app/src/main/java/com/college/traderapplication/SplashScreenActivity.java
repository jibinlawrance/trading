package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.college.admin.AdminMainActivity;
import com.college.util.AppController;
import com.college.util.SharedPreference;
import com.college.vendor.MenuActivity;


public class SplashScreenActivity extends AppCompatActivity {


    String value="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreference.initialize(getApplicationContext());
        AppController.initialize(getApplicationContext());
        if (SharedPreference.contains("value")){
            value=SharedPreference.get("value");
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(value.equals("1")){
                    Intent intent=new Intent(SplashScreenActivity.this, AdminMainActivity.class);
                    startActivity(intent);
                    finish();
                }else if(value.equals("2")){
                    Intent intent=new Intent(SplashScreenActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                }else if(value.equals("3")){
                    Intent intent=new Intent(SplashScreenActivity.this, UserMenuActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(SplashScreenActivity.this, MainMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },3000);
    }
}
