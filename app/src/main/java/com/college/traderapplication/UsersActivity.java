package com.college.traderapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.college.adapter.HighestBidAdapter;
import com.college.pojo.HighestBid;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    HighestBidAdapter adapter;
    ArrayList<HighestBid> list;
    LinearLayoutManager layoutManager;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        recyclerView=findViewById(R.id.recycler_users_list);
        getSupportActionBar().setTitle("Highest Bids");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<>();
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
//        list.add(new HighestBid("John Mathew","HP Laptop","25,000 Rs"));
        adapter=new HighestBidAdapter(getApplicationContext(),list);
        recyclerView.setAdapter(adapter);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
